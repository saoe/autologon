﻿/*
Autologon: Small utility program to modify the Windows Autologon settings in the Registry.  
See the accompanied ReadMe.md file for more information and help!

Copyright © 2018 Sascha Offe <so@saoe.net>
Published under the ISC license (see the full text at the end of this file).

#002 2018-05-01 (so) Switched license from MIT ('Expat') to the ISC license: Shorter, simpler, but basically the same.
#001 2018-04-21 (so) Project's birthday; creation of the first executable.
*/

using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices.AccountManagement;

namespace Autologon
{

static class Constants
{
	public const String PROGRAMNAME = "Autologon";
	public const String COPYRIGHT = "© 2018 Sascha Offe";
	public const String VERSION = "1.1";
	public const String LICENSE = "ISC license";
	public const String URL = "http://www.saoe.net/";
}

class AutoLogonRegistryValues
{
	RegistryKey regkey;

	public String DefaultUserNameValue;
	public String DefaultPasswordValue ;
	public String DefaultDomainNameValue;
	public String ForceAutoLogonValue;

	public AutoLogonRegistryValues ()
	{
		try
		{
			// --- Productive ---
			// Note: Trying to open a non-existing key returns 'null'!
			regkey = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon");

			// --- Testing ---
			//regkey = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\saoe\Test"); // CreateSubKey() -> will create _or_ open.
		}
		catch (UnauthorizedAccessException uae)
		{
			throw;
		}


		ForceAutoLogonValue = "0";
	}

	public void clear ()
	{
		//regkey.SetValue("DefaultUserName", "", RegistryValueKind.String);
		regkey.DeleteValue("DefaultPassword", false);
		//regkey.SetValue("DefaultDomainName", "", RegistryValueKind.String);
		regkey.SetValue("AutoAdminLogon", "0", RegistryValueKind.String);
		regkey.DeleteValue("ForceAutoLogon", false);
	}

	public void set ()
	{
		regkey.SetValue("DefaultUserName", DefaultUserNameValue, RegistryValueKind.String);
		regkey.SetValue("DefaultPassword", DefaultPasswordValue, RegistryValueKind.String);
		regkey.SetValue("DefaultDomainName", DefaultDomainNameValue, RegistryValueKind.String);
		regkey.SetValue("AutoAdminLogon", "1", RegistryValueKind.String);
		regkey.SetValue("ForceAutoLogon", ForceAutoLogonValue, RegistryValueKind.String);
	}
}


class MainForm : Form
{
	AutoLogonRegistryValues regval;
	String domain;

	String locale_suffix;

	private Label username_label = new Label() ;
	private TextBox username_input = new TextBox();

	private Label password_label = new Label();
	private TextBox password_input = new TextBox();

	private Label password_confirmation_label = new Label();
	private TextBox password_confirmation_input = new TextBox();

	private PictureBox status_icon_notstarted = new PictureBox();
	private PictureBox status_icon_ok = new PictureBox();
	private PictureBox status_icon_invalid = new PictureBox();
	private PictureBox status_icon_info = new PictureBox();

	private Button check_account_button = new Button();
	private Button activate_autologon_button = new Button();
	private Button clear_autologon_button = new Button();

	private CheckBox forceautologon_checkbox = new CheckBox();

	private RichTextBox log = new RichTextBox();
	
	FlowLayoutPanel panel = new FlowLayoutPanel();
	TableLayoutPanel table1 = new TableLayoutPanel();
	
	public MainForm ()
	{
		// Language strings for MUI programs are by default split into separate resource DLLs in .NET.
		// There are workaround workflows for this, but in this case, we just make it simple and put
		// all strings in the one and only (embedded) resources.resx, differentiating the translated pieces
		// with a suffix...
		if (System.Globalization.CultureInfo.CurrentUICulture.ToString().StartsWith("de"))
		{
			locale_suffix = "_de";
		}
		else
		{
			locale_suffix = "";
		}
		
		this.AutoSize = true;
		this.MaximizeBox = false;
		this.MinimizeBox = false;
		this.FormBorderStyle = FormBorderStyle.FixedDialog;
		this.Text = Constants.PROGRAMNAME;
		this.StartPosition = FormStartPosition.CenterScreen;
		this.Font = SystemFonts.MessageBoxFont; // Workaround to get some kind of value for the system default font.

		forceautologon_checkbox.Click += new EventHandler(this.forceautologon_checkbox_Click);
		check_account_button.Click += new EventHandler(this.check_account_button_Click);
		activate_autologon_button.Click += new EventHandler(this.activate_autologon_button_Click);
		clear_autologon_button.Click += new EventHandler(this.disable_autologon_button_Click);
		status_icon_info.Click += new EventHandler(this.status_icon_info_Click);
		
		log.LinkClicked += new LinkClickedEventHandler(this.log_LinkClicked);

		try
		{
			regval = new AutoLogonRegistryValues();
		}
		catch (UnauthorizedAccessException uae)
		{
			username_input.Enabled = false;
			password_input.Enabled = false;
			password_confirmation_input.Enabled = false;
			check_account_button.Enabled = false;
			forceautologon_checkbox.Enabled = false;
			clear_autologon_button.Enabled = false;
			
			log.Text = Properties.Resources.ResourceManager.GetString("msg_administrator_rights_required" + locale_suffix);
		}
		
		username_input.TextChanged += new EventHandler(xyz_input_TextChanged);
		password_input.TextChanged += new EventHandler(xyz_input_TextChanged);
		password_confirmation_input.TextChanged += new EventHandler(xyz_input_TextChanged);
		Load += new EventHandler(MainForm_Load);
	}

	private void MainForm_Load (object sender, System.EventArgs e)
	{
		username_label.Text = Properties.Resources.ResourceManager.GetString("username" + locale_suffix) + ":";
		username_label.TextAlign = ContentAlignment.MiddleLeft;
		username_label.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left);
		username_label.AutoSize = true;
		
		username_input.Dock = DockStyle.Fill;

		password_label.Text = Properties.Resources.ResourceManager.GetString("password" + locale_suffix) + ":";
		password_label.TextAlign = ContentAlignment.MiddleLeft;
		password_label.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left);
		password_label.AutoSize = true;

		password_input.PasswordChar = '\u2022';
		password_input.Dock = DockStyle.Fill;
		
		password_confirmation_label.Text = Properties.Resources.ResourceManager.GetString("password_confirmation" + locale_suffix) + ":";
		password_confirmation_label.TextAlign = ContentAlignment.MiddleLeft;
		password_confirmation_label.Anchor = (AnchorStyles.Bottom | AnchorStyles.Top | AnchorStyles.Left);
		password_confirmation_label.AutoSize = true;

		password_confirmation_input.PasswordChar = '\u2022';
		password_confirmation_input.Dock = DockStyle.Fill;

		FlowLayoutPanel check_account_layout = new FlowLayoutPanel();
		check_account_layout.FlowDirection = FlowDirection.LeftToRight;
		
		check_account_layout.AutoSize = true;
		check_account_layout.AutoSizeMode = AutoSizeMode.GrowAndShrink;
		check_account_layout.WrapContents = false;

		check_account_button.Text = Properties.Resources.ResourceManager.GetString("check_account" + locale_suffix);
		check_account_button.Enabled = false;
		check_account_button.AutoSize = true;
		check_account_button.AutoSizeMode = AutoSizeMode.GrowAndShrink;
		check_account_button.AutoEllipsis = false;
		
		status_icon_notstarted.Image = Properties.Resources.StatusNotStarted_16x;
		status_icon_notstarted.SizeMode = PictureBoxSizeMode.AutoSize;
		status_icon_notstarted.Anchor = AnchorStyles.None;
		status_icon_notstarted.Show();
		
		status_icon_ok.Image = Properties.Resources.StatusOK_16x;
		status_icon_ok.SizeMode = PictureBoxSizeMode.AutoSize;
		status_icon_ok.Anchor = AnchorStyles.None;
		status_icon_ok.Hide();
		
		status_icon_invalid.Image = Properties.Resources.StatusInvalid_16x;
		status_icon_invalid.SizeMode = PictureBoxSizeMode.AutoSize;
		status_icon_invalid.Anchor = AnchorStyles.None;
		status_icon_invalid.Hide();
		
		status_icon_info.Image = Properties.Resources.StatusInfoTip_16x;
		status_icon_info.SizeMode = PictureBoxSizeMode.AutoSize;
		status_icon_info.Anchor = AnchorStyles.Right;
		status_icon_info.Show();

		check_account_layout.Controls.Add(check_account_button);
		check_account_layout.Controls.Add(status_icon_notstarted);
		check_account_layout.Controls.Add(status_icon_ok);
		check_account_layout.Controls.Add(status_icon_invalid);

		activate_autologon_button.Text = Properties.Resources.ResourceManager.GetString("activate_autologon" + locale_suffix);
		activate_autologon_button.AutoSize = true;
		activate_autologon_button.AutoSizeMode = AutoSizeMode.GrowAndShrink;
		activate_autologon_button.AutoEllipsis = false;
		activate_autologon_button.Enabled = false;
		
		clear_autologon_button.Text = Properties.Resources.ResourceManager.GetString("clear_autologon" + locale_suffix);
		clear_autologon_button.AutoSize = true;
		clear_autologon_button.AutoSizeMode = AutoSizeMode.GrowAndShrink;
		clear_autologon_button.AutoEllipsis = false;
		
		table1.AutoSize = true;
		table1.Dock = DockStyle.Fill;
		table1.ColumnCount = 2;
		table1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
		table1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));

		table1.Controls.Add(username_label, 0, 0);
		table1.Controls.Add(username_input, 1, 0);
		table1.Controls.Add(password_label, 0, 1);
		table1.Controls.Add(password_input, 1, 1);
		table1.Controls.Add(password_confirmation_label, 0, 2);
		table1.Controls.Add(password_confirmation_input, 1, 2);

		panel.FlowDirection = FlowDirection.TopDown;
		panel.WrapContents = false;
		panel.AutoScroll = true;
		panel.AutoSize = true;

		forceautologon_checkbox.Text = Properties.Resources.ResourceManager.GetString("force_autologon" + locale_suffix);
		forceautologon_checkbox.AutoSize = true;
		ToolTip forceautologon_checkbox_tooltip = new ToolTip();
		forceautologon_checkbox_tooltip.SetToolTip(forceautologon_checkbox, Properties.Resources.ResourceManager.GetString("force_autologon_tooltip" + locale_suffix));
		
		log.ReadOnly = true;
		log.Multiline = true;
		log.BorderStyle = BorderStyle.Fixed3D;
		log.Width = this.ClientSize.Width - this.Margin.All - log.Margin.All;
		log.Height = 80;

		panel.Controls.Add(table1);
		panel.Controls.Add(check_account_layout);
		panel.Controls.Add(forceautologon_checkbox);
		panel.Controls.Add(activate_autologon_button);
		panel.Controls.Add(clear_autologon_button);
		panel.Controls.Add(log);
		panel.Controls.Add(status_icon_info);

		this.Controls.Add(panel);
	}


	private void forceautologon_checkbox_Click (object sender, System.EventArgs e)
	{
		if (forceautologon_checkbox.Checked)  
			regval.ForceAutoLogonValue = "1";
		else  
			regval.ForceAutoLogonValue = "0";
	}


	private void activate_autologon_button_Click (object sender, System.EventArgs e)
	{
		regval.set();
	}
	

	private void disable_autologon_button_Click (object sender, System.EventArgs e)
	{
		regval.clear();
	}


	private void check_account_button_Click (object sender, System.EventArgs e)
	{
		if (string.IsNullOrEmpty(username_input.Text) || string.IsNullOrEmpty(password_input.Text) || string.IsNullOrEmpty(password_confirmation_input.Text))
		{
			log.Text = Properties.Resources.ResourceManager.GetString("msg_provide_username_and_password" + locale_suffix);
		}
		else
		{
			if (password_input.Text != password_confirmation_input.Text)
			{
				log.Text = Properties.Resources.ResourceManager.GetString("msg_passwords_do_not_match" + locale_suffix);
			}
			else
			{
				ContextType context = ContextType.Domain;
				
				try
				{
					this.domain = Domain.GetComputerDomain().ToString();
				}
				catch (ActiveDirectoryObjectNotFoundException adonfe)
				{
					this.domain = null;
					context = ContextType.Machine;
				}

				using (PrincipalContext pc = new PrincipalContext(context))
				{
					bool isValid = pc.ValidateCredentials(username_input.Text, password_input.Text);

					if (isValid)
					{
						status_icon_notstarted.Hide();
						status_icon_ok.Show();
						
						String str = Properties.Resources.ResourceManager.GetString("msg_account_is_valid" + locale_suffix);;

						activate_autologon_button.Enabled = isValid;

						if (this.domain == null)
						{
							regval.DefaultDomainNameValue = "";
							str += " (" + System.Net.Dns.GetHostEntry("localhost").HostName + ").";
						}
						else
						{
							regval.DefaultDomainNameValue = this.domain;
							str += " (" + this.domain + ").";
						}
					
						log.Text = str;
					}
					else
					{
						status_icon_notstarted.Hide();
						status_icon_invalid.Show();
						log.Text = Properties.Resources.ResourceManager.GetString("msg_account_is_invalid" + locale_suffix);;
						activate_autologon_button.Enabled = isValid;
					}
				}
			}
		}
	}


	private void xyz_input_TextChanged(object sender, EventArgs e)
	{
		log.Clear();
		status_icon_ok.Hide();
		status_icon_invalid.Hide();
		status_icon_notstarted.Show();
		
		activate_autologon_button.Enabled = false;

		if (string.IsNullOrEmpty(username_input.Text) || string.IsNullOrEmpty(password_input.Text) || string.IsNullOrEmpty(password_confirmation_input.Text))
		{
			check_account_button.Enabled = false;
		}
		else
		{
			check_account_button.Enabled = true;
			
			regval.DefaultUserNameValue = username_input.Text;
			regval.DefaultPasswordValue = password_input.Text;
		}
	}


	private void status_icon_info_Click (object sender, System.EventArgs e)
	{
		log.Text = Constants.PROGRAMNAME + " " + Constants.VERSION + "\r\n" +
		Constants.COPYRIGHT + " \u00B7 " + Constants.URL + "\r\n" +
		"Published under the " + Constants.LICENSE + ".";
	}

	
	// Event raised from RichTextBox when user clicks on a link.
	private void log_LinkClicked(object sender, LinkClickedEventArgs e)
	{
		// Opens default browser.
		System.Diagnostics.Process.Start(e.LinkText);
	}
}


class Program
{
	public static void Main (string[] args) 
	{
		Application.Run(new MainForm());
	}
}

} // Namespace.


/******************************************************************************************
*                                                                                         *
* The ISC license <https://opensource.org/licenses/ISC>:                                  *
*                                                                                         *
* Permission to use, copy, modify, and/or distribute this software for any                *
* purpose with or without  fee is hereby granted, provided that the above                 *
* copyright notice and this permission notice appear in all copies.                       *
*                                                                                         *
* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES                *
* WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY		  *
* AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,  *
* OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR  *
* PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING *
* OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.                   *                                                              *
*                                                                                         *
******************************************************************************************/
