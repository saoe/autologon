﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Autologon.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Autologon.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activate Autologon (on next boot).
        /// </summary>
        internal static string activate_autologon {
            get {
                return ResourceManager.GetString("activate_autologon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Autologon aktivieren (beim nächsten Start).
        /// </summary>
        internal static string activate_autologon_de {
            get {
                return ResourceManager.GetString("activate_autologon_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Check account.
        /// </summary>
        internal static string check_account {
            get {
                return ResourceManager.GetString("check_account", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Konto prüfen.
        /// </summary>
        internal static string check_account_de {
            get {
                return ResourceManager.GetString("check_account_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear Autologon settings.
        /// </summary>
        internal static string clear_autologon {
            get {
                return ResourceManager.GetString("clear_autologon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Autologon-Einstellungen löschen.
        /// </summary>
        internal static string clear_autologon_de {
            get {
                return ResourceManager.GetString("clear_autologon_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Force Auto Logon.
        /// </summary>
        internal static string force_autologon {
            get {
                return ResourceManager.GetString("force_autologon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Autologon erzwingen.
        /// </summary>
        internal static string force_autologon_de {
            get {
                return ResourceManager.GetString("force_autologon_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logs the account immediately back on after logoff..
        /// </summary>
        internal static string force_autologon_tooltip {
            get {
                return ResourceManager.GetString("force_autologon_tooltip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Meldet das Konto nach Abmeldung sofort wieder an..
        /// </summary>
        internal static string force_autologon_tooltip_de {
            get {
                return ResourceManager.GetString("force_autologon_tooltip_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error: The account data is invalid!.
        /// </summary>
        internal static string msg_account_is_invalid {
            get {
                return ResourceManager.GetString("msg_account_is_invalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fehler: Benutzerkontodaten sind ungültig!.
        /// </summary>
        internal static string msg_account_is_invalid_de {
            get {
                return ResourceManager.GetString("msg_account_is_invalid_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK: The account data is valid..
        /// </summary>
        internal static string msg_account_is_valid {
            get {
                return ResourceManager.GetString("msg_account_is_valid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK: Die Benutzerkontodaten sind in Ordnung.
        /// </summary>
        internal static string msg_account_is_valid_de {
            get {
                return ResourceManager.GetString("msg_account_is_valid_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error: Local administrator rights are required!.
        /// </summary>
        internal static string msg_administrator_rights_required {
            get {
                return ResourceManager.GetString("msg_administrator_rights_required", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error: Es werden lokale Administratorrechte benötigt!.
        /// </summary>
        internal static string msg_administrator_rights_required_de {
            get {
                return ResourceManager.GetString("msg_administrator_rights_required_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error: The password inputs do not match!.
        /// </summary>
        internal static string msg_passwords_do_not_match {
            get {
                return ResourceManager.GetString("msg_passwords_do_not_match", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fehler: Die Passwörter stimmen nicht überein!.
        /// </summary>
        internal static string msg_passwords_do_not_match_de {
            get {
                return ResourceManager.GetString("msg_passwords_do_not_match_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide username and password!.
        /// </summary>
        internal static string msg_provide_username_and_password {
            get {
                return ResourceManager.GetString("msg_provide_username_and_password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bitte Benutzername und Passwort angeben!.
        /// </summary>
        internal static string msg_provide_username_and_password_de {
            get {
                return ResourceManager.GetString("msg_provide_username_and_password_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        internal static string password {
            get {
                return ResourceManager.GetString("password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password confirmation.
        /// </summary>
        internal static string password_confirmation {
            get {
                return ResourceManager.GetString("password_confirmation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passwortbestätigung.
        /// </summary>
        internal static string password_confirmation_de {
            get {
                return ResourceManager.GetString("password_confirmation_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passwort.
        /// </summary>
        internal static string password_de {
            get {
                return ResourceManager.GetString("password_de", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap StatusInfoTip_16x {
            get {
                object obj = ResourceManager.GetObject("StatusInfoTip_16x", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap StatusInvalid_16x {
            get {
                object obj = ResourceManager.GetObject("StatusInvalid_16x", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap StatusNotStarted_16x {
            get {
                object obj = ResourceManager.GetObject("StatusNotStarted_16x", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap StatusOK_16x {
            get {
                object obj = ResourceManager.GetObject("StatusOK_16x", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username.
        /// </summary>
        internal static string username {
            get {
                return ResourceManager.GetString("username", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Benutzername.
        /// </summary>
        internal static string username_de {
            get {
                return ResourceManager.GetString("username_de", resourceCulture);
            }
        }
    }
}
