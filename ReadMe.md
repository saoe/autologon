# Autologon

A small utility program (written in C#/.NET) to modify the Windows Autologon settings in the registry with a GUI.

[**Download** the (zipped) executable file](https://bitbucket.org/saoe/autologon/downloads/) (for 64-bit systems only)  

[Project page/Code Repository](https://bitbucket.org/saoe/autologon) (this here)

## Features

*  UI language: Default is english, but on systems set to a german user interface, translated text will be shown.
*  _"Check account"_ tests the validity of the entered username and password against a local machine or the (current) domain.
*  _"Force Autlogon"_ enforces that the account will immediately be logged on again after a logoff; useful for stations that should act like 'kiosk terminals'.
* _"Clear Autologon settings"_ removed all autologon traces from the registry (regardless who set it).

![Screenshot1](Screenshot_1.png)

## Notes

This program modifies the Windows Registry -- for that reason, it requires local admin rights to run.  
Please note the following:

- To create the registry keys in the right branch, build it for a x64 (64-bit) target platform if you intend to run it on a 64-bit Windows.  
Otherwise the modifications happen in the 32-bit branch (below 'WOW6432Node') and have no effect on 64-bit systems!

--------------------------------------------------------------------------------

Copyright © 2018 [Sascha Offe](http://www.saoe.net/).  
Published under the [ISC license](https://opensource.org/licenses/ISC) (follow the link or look in to the source code files for the full text).
